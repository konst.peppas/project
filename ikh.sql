-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: ikh
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `EmployeePayrolls`
--

DROP TABLE IF EXISTS `EmployeePayrolls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EmployeePayrolls` (
  `PayableId` int(11) NOT NULL AUTO_INCREMENT,
  `EmployeeId` int(11) NOT NULL,
  `PayrollId` int(11) NOT NULL,
  `Salary` decimal(12,2) DEFAULT NULL,
  `Tax` decimal(12,2) DEFAULT NULL,
  `NetPayable` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`PayableId`),
  KEY `fk_Employees_has_Payroles_Payroles1_idx` (`PayrollId`),
  KEY `fk_Employees_has_Payroles_Employees1_idx` (`EmployeeId`),
  CONSTRAINT `fk_Employees_has_Payroles_Employees1` FOREIGN KEY (`EmployeeId`) REFERENCES `Employees` (`EmployeeId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Employees_has_Payroles_Payroles1` FOREIGN KEY (`PayrollId`) REFERENCES `Payrolls` (`PayrollID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EmployeePayrolls`
--

LOCK TABLES `EmployeePayrolls` WRITE;
/*!40000 ALTER TABLE `EmployeePayrolls` DISABLE KEYS */;

/*!40000 ALTER TABLE `EmployeePayrolls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Employees`
--

DROP TABLE IF EXISTS `Employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Employees` (
  `EmployeeId` int(11) NOT NULL AUTO_INCREMENT,
  `EmployeeCode` int(11) NOT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `Sex` char(1) DEFAULT NULL,
  `Photo` varchar(255) DEFAULT NULL,
  `Salary` decimal(12,2) DEFAULT NULL,
  `Active` bit(1) NOT NULL,
  PRIMARY KEY (`EmployeeId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Employees`
--

LOCK TABLES `Employees` WRITE;
/*!40000 ALTER TABLE `Employees` DISABLE KEYS */;

/*!40000 ALTER TABLE `Employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Payrolls`
--

DROP TABLE IF EXISTS `Payrolls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Payrolls` (
  `PayrollID` int(11) NOT NULL AUTO_INCREMENT,
  `Year` int(11) NOT NULL,
  `Month` int(11) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PayrollID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Payrolls`
--

LOCK TABLES `Payrolls` WRITE;
/*!40000 ALTER TABLE `Payrolls` DISABLE KEYS */;

/*!40000 ALTER TABLE `Payrolls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SeminarTypes`
--

DROP TABLE IF EXISTS `SeminarTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SeminarTypes` (
  `SeminarTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `SeminarTypeCode` varchar(12) NOT NULL,
  `Name` varchar(128) NOT NULL,
  `Notes` varchar(255) DEFAULT NULL,
  `Active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`SeminarTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SeminarTypes`
--

LOCK TABLES `SeminarTypes` WRITE;
/*!40000 ALTER TABLE `SeminarTypes` DISABLE KEYS */;

/*!40000 ALTER TABLE `SeminarTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Seminars`
--

DROP TABLE IF EXISTS `Seminars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Seminars` (
  `SeminarId` int(11) NOT NULL AUTO_INCREMENT,
  `EmployeeId` int(11) NOT NULL,
  `SeminarTypeId` int(11) NOT NULL,
  `SeminarDate` datetime NOT NULL,
  `Notes` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`SeminarId`),
  UNIQUE KEY `unique_employee_seminarType` (`SeminarTypeId`,`EmployeeId`),
  KEY `fk_Employees_has_SeminarTypes_Employees_idx` (`EmployeeId`),
  KEY `fk_Seminars_1_idx` (`SeminarTypeId`),
  CONSTRAINT `fk_Employees_has_SeminarTypes_Employees` FOREIGN KEY (`EmployeeId`) REFERENCES `Employees` (`EmployeeId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Seminars_1` FOREIGN KEY (`SeminarTypeId`) REFERENCES `SeminarTypes` (`SeminarTypeId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Seminars`
--

LOCK TABLES `Seminars` WRITE;
/*!40000 ALTER TABLE `Seminars` DISABLE KEYS */;
/*!40000 ALTER TABLE `Seminars` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-05 17:00:08
