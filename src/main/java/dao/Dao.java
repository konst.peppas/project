package dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import entities.*;

@Repository
public class Dao {
	@Autowired
	private EntityManager em;

	//Empoyee CRUD
	public List<Employee> getAllEmployees(){
		TypedQuery<Employee> q = em.createNamedQuery("Employee.findAll",Employee.class);
		List<Employee> list = q.getResultList();
		return list;
	}

	public Employee getEmployee(int id){
		return em.find(Employee.class, id);
	}

	@Transactional
	public void saveEmployee(Employee emp) {
		em.merge(emp);
	}

	@Transactional
	public void deleteEmployee(int id) {
		Employee emp = em.find(Employee.class,id);
		em.remove(emp);
	}

	//SeminarTypes CRUD
	public List<SeminarType> getAllSeminarTypes(){
		TypedQuery<SeminarType> q = em.createNamedQuery("SeminarType.findAll",SeminarType.class);
		List<SeminarType> list = q.getResultList();
		return list;
	}

	@Transactional
	public void saveSeminarType(SeminarType st) {
		em.merge(st);
	}

	@Transactional
	public void deleteSeminarType(int id) {
		SeminarType st = em.find(SeminarType.class, id);
		em.remove(st);
	}

	// Seminars CRUD//

	public List<Seminar> getAllSeminars(){
		TypedQuery<Seminar> q = em.createNamedQuery("Seminar.findALl",Seminar.class);
		return q.getResultList();
	}

	@Transactional
	public void saveSeminar(Seminar sem, Employee emp, SeminarType st) {
		sem.setEmployee(emp);
		sem.setSeminarType(st);
		em.persist(sem);
	}

	@Transactional
	public void deleteSeminar(int empId,int semTypId) {
		TypedQuery<Seminar> q = em.createQuery("select sem from Seminar sem "
				+ "where sem.employee.employeeId = :empId and sem.seminarType.seminarTypeId = :semTypId",Seminar.class);
		Seminar sem = q.setParameter("empId", empId).setParameter("semTypId",semTypId).getSingleResult();
		em.remove(sem);
	}

	public List<Seminar> getEmployeeSeminars(int empId){
		TypedQuery<Seminar> q = em.createQuery("select s from Seminar s join fetch s.seminarType where s.employee.id = :empid",Seminar.class);
		return q.setParameter("empid", empId).getResultList();
	}

	@Transactional
	public void createEmployeeSeminar(int empId, int semTypId, Seminar sem) {
		Employee emp = em.find(Employee.class, empId);
		SeminarType semTyp = em.find(SeminarType.class, semTypId);

		sem.setEmployee(emp);
		sem.setSeminarType(semTyp);
		em.persist(sem);
	}
	
	//seminar types that no employee have been attended
	public List<SeminarType> getSeminarTypesForEmployee(int employeeId){
		TypedQuery<SeminarType> query = em.createQuery("select st from SeminarType st where st not in "
				+ "(select s from Seminar s where s.employee.id = :emplId)",SeminarType.class);
		return query.setParameter("emplId", employeeId).getResultList();
	}
	
	//// Payrolls ////
	
	public List<EmployeePayroll> getEmployeePayrolls(int emplId){
		TypedQuery<EmployeePayroll> q = em.createQuery("select ep from EmployeePayroll ep join fetch ep.payroll p join fetch ep.employee e where ep.employee.employeeId = :empId",EmployeePayroll.class);
		return q.setParameter("empId", emplId).getResultList();
	}
	
	
	public List<Payroll> getAllPayrolls(){
		TypedQuery<Payroll> q = em.createQuery("select p from Payroll p order by p.month, p.year desc",Payroll.class);
		return q.getResultList();
	}
	
	@Transactional
	public void deletePayroll(int id) {
		Payroll p = em.find(Payroll.class, id);
			em.remove(p);
	}
	
	@Transactional
	public void savePayroll(Payroll payroll) {
		payroll.setPayrollId(0);
		em.persist(payroll);
	}
	
	@Transactional
	public void deletePayrollRecords(Payroll p) {
		StoredProcedureQuery q = em.createNamedStoredProcedureQuery("deletePayrollRecords");
		q.setParameter("id", p.getPayrollId());
	}
	
	@Transactional
	public void createEmployeePayroll(EmployeePayroll ep) {
		em.persist(ep);
	}

	
}
