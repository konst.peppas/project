package endpoint;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import entities.Employee;
import entities.EmployeePayroll;
import entities.Payroll;
import entities.Seminar;
import entities.SeminarType;
import services.PayrollService;
import dao.Dao;

@RestController
@RequestMapping("")
public class EmployeeController {

	@Autowired
	Dao dao;
	//// Employee ////
	@RequestMapping(value = "employees", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Employee> hello() {		
		return dao.getAllEmployees();
	}
	
	@RequestMapping(value = "employees/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Employee getEmployee(@PathVariable int id) {		
		return dao.getEmployee(id);
	}
	
	@RequestMapping(value = "/employees", method = RequestMethod.POST)
	public void createEmployee(@RequestBody Employee emp) {		
		dao.saveEmployee(emp);
	}
	
	@RequestMapping(value = "/employees/{id}", method = RequestMethod.PUT)
	public void createEmployee(@RequestBody Employee emp,@PathVariable int id) {		
		emp.setEmployeeId(id);
		dao.saveEmployee(emp);
	}
	
	@RequestMapping(value = "/employees/{id}", method = RequestMethod.DELETE)
	public void deleteEmployee(@PathVariable int id) {		
		dao.deleteEmployee(id);
	}
	
	//// Seminar Types ////
	@RequestMapping(value = "/seminarTypes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SeminarType> getAllSeminarTypes() {
		return dao.getAllSeminarTypes();
	}
	
	@RequestMapping(value = "/seminarTypes", method = RequestMethod.POST)
	public void createSeminarType(@RequestBody SeminarType st) {
		dao.saveSeminarType(st);
	}
	
	@RequestMapping(value = "/seminarTypes/{id}", method = RequestMethod.DELETE)
	public void deleteSeminarType(@PathVariable int id) {		
		dao.deleteSeminarType(id); 
	}
	//// Seminars ////
	
	@RequestMapping(value = "/employees/{id}/seminars", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Seminar> getEmployeeSeminars(@PathVariable int id) {
		return dao.getEmployeeSeminars(id);
	}
	
	@RequestMapping(value = "/employees/{emplId}/seminars/{semTypId}", method = RequestMethod.POST)
	public void createSeminarType(@PathVariable int emplId, @PathVariable int semTypId, @RequestBody Seminar sem) {
		dao.createEmployeeSeminar(emplId, semTypId, sem);
	}
	
	@RequestMapping(value = "/employees/{emplId}/seminars/{semTypId}", method = RequestMethod.DELETE)
	public void createSeminarType(@PathVariable int emplId, @PathVariable int semTypId) {
		dao.deleteSeminar(emplId, semTypId);
	}
	
	//// EmployeePayrolls ////
	
	@RequestMapping(value = "/employees/{emplId}/payrolls", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<EmployeePayroll> getEmployeePayrolls(@PathVariable int emplId) {
		return dao.getEmployeePayrolls(emplId);
	}
	 
	
	@RequestMapping(value = "/payrolls", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Payroll> getAllPayrolls() {
		return dao.getAllPayrolls();
	}

	@RequestMapping(value = "/payrolls/{id}", method = RequestMethod.DELETE)
	public void deletePayroll(@PathVariable int id) {
		dao.deletePayroll(id);
	}
	 
	@Autowired
	private PayrollService payrollService;
	
	@RequestMapping(value = "/payrolls", method = RequestMethod.POST)
	public void createPayroll(@RequestBody Payroll payroll) {
		dao.savePayroll(payroll);
		payrollService.calculatePayroll(payroll);
	}

	
}