package entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the Employees database table.
 * 
 */
@Entity
@Table(name="Employees")
@NamedQuery(name="Employee.findAll", query="SELECT e FROM Employee e")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="EmployeeId")
	private int employeeId;

	@Column(name="Active")
	private byte active;

	@Column(name="EmployeeCode")
	private int employeeCode;

	@Column(name="FirstName")
	private String firstName;

	@Column(name="LastName")
	private String lastName;

	@Column(name="MiddleName")
	private String middleName;

	@Column(name="Photo")
	private String photo;

	@Column(name="Salary")
	private BigDecimal salary;

	@Column(name="Sex")
	private String sex;

	@JsonIgnore
	//bi-directional many-to-one association to EmployeePayroll
	@OneToMany(mappedBy="employee")
	private List<EmployeePayroll> employeePayrolls;

	@JsonIgnore 
	//bi-directional many-to-one association to Seminar
	@OneToMany(mappedBy="employee")
	private List<Seminar> seminars;

	public Employee() {
	}

	public int getEmployeeId() {
		return this.employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public int getEmployeeCode() {
		return this.employeeCode;
	}

	public void setEmployeeCode(int employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public BigDecimal getSalary() {
		return this.salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public List<EmployeePayroll> getEmployeePayrolls() {
		return this.employeePayrolls;
	}

	public void setEmployeePayrolls(List<EmployeePayroll> employeePayrolls) {
		this.employeePayrolls = employeePayrolls;
	}

	public EmployeePayroll addEmployeePayroll(EmployeePayroll employeePayroll) {
		getEmployeePayrolls().add(employeePayroll);
		employeePayroll.setEmployee(this);

		return employeePayroll;
	}

	public EmployeePayroll removeEmployeePayroll(EmployeePayroll employeePayroll) {
		getEmployeePayrolls().remove(employeePayroll);
		employeePayroll.setEmployee(null);

		return employeePayroll;
	}

	public List<Seminar> getSeminars() {
		return this.seminars;
	}

	public void setSeminars(List<Seminar> seminars) {
		this.seminars = seminars;
	}

	public Seminar addSeminar(Seminar seminar) {
		getSeminars().add(seminar);
		seminar.setEmployee(this);

		return seminar;
	}

	public Seminar removeSeminar(Seminar seminar) {
		getSeminars().remove(seminar);
		seminar.setEmployee(null);

		return seminar;
	}

}