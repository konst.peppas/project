package entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the EmployeePayrolls database table.
 * 
 */
@Entity
@Table(name="EmployeePayrolls",
    uniqueConstraints=
        @UniqueConstraint(columnNames={"EmployeeId", "PayrollId"})
)
@NamedQuery(name="EmployeePayroll.findAll", query="SELECT e FROM EmployeePayroll e")
public class EmployeePayroll implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PayableId")
	private int payableId;

	@Column(name="NetPayable")
	private BigDecimal netPayable;

	@Column(name="Salary")
	private BigDecimal salary;

	@Column(name="Tax")
	private BigDecimal tax;

	//bi-directional many-to-one association to Employee
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="EmployeeId")
	private Employee employee;

	//bi-directional many-to-one association to Payroll
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PayrollId")
	private Payroll payroll;

	public EmployeePayroll() {
	}

	public int getPayableId() {
		return this.payableId;
	}

	public void setPayableId(int payableId) {
		this.payableId = payableId;
	}

	public BigDecimal getNetPayable() {
		return this.netPayable;
	}

	public void setNetPayable(BigDecimal netPayable) {
		this.netPayable = netPayable;
	}

	public BigDecimal getSalary() {
		return this.salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public BigDecimal getTax() {
		return this.tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Payroll getPayroll() {
		return this.payroll;
	}

	public void setPayroll(Payroll payroll) {
		this.payroll = payroll;
	}

}