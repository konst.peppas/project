package entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the Payrolls database table.
 * 
 */
@Entity
@Table(name="Payrolls")
@NamedQuery(name="Payroll.findAll", query="SELECT p FROM Payroll p")
@NamedStoredProcedureQueries({
	  @NamedStoredProcedureQuery(
	    name = "deletePayrollRecords", 
	    procedureName = "deletePayrollRecords", 
	    parameters = { 
	        @StoredProcedureParameter(
	          name = "id", 
	          type = Integer.class, 
	          mode = ParameterMode.IN
	        ) 
	    }) 
})
public class Payroll implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PayrollID")
	private int payrollId;

	@Column(name="Month")
	private int month;

	@Column(name="Description")
	private String description;

	@Column(name="Year")
	private int year;

	//bi-directional many-to-one association to EmployeePayroll
	@JsonIgnore
	@OneToMany(mappedBy="payroll")
	private List<EmployeePayroll> employeePayrolls;

	public Payroll() {
	}

	public int getPayrollId() {
		return this.payrollId;
	}

	public void setPayrollId(int payrollID) {
		this.payrollId = payrollID;
	}

	public int getMonth() {
		return this.month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String payrollscol) {
		this.description = payrollscol;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public List<EmployeePayroll> getEmployeePayrolls() {
		return this.employeePayrolls;
	}

	public void setEmployeePayrolls(List<EmployeePayroll> employeePayrolls) {
		this.employeePayrolls = employeePayrolls;
	}

	public EmployeePayroll addEmployeePayroll(EmployeePayroll employeePayroll) {
		getEmployeePayrolls().add(employeePayroll);
		employeePayroll.setPayroll(this);

		return employeePayroll;
	}

	public EmployeePayroll removeEmployeePayroll(EmployeePayroll employeePayroll) {
		getEmployeePayrolls().remove(employeePayroll);
		employeePayroll.setPayroll(null);

		return employeePayroll;
	}

}