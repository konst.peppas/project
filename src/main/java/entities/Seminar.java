package entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;


/**
 * The persistent class for the Seminars database table.
 * 
 */
@Entity
@Table(name="Seminars",
	uniqueConstraints=
		@UniqueConstraint(columnNames={"EmployeeId", "SeminarTypeId"})
)
@NamedQuery(name="Seminar.findAll", query="SELECT s FROM Seminar s")
public class Seminar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SeminarId")
	private int seminarId;

	@Column(name="Notes")
	private String notes;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SeminarDate")
	private Date seminarDate;

	@JsonIgnore
	//bi-directional many-to-one association to Employee
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="EmployeeId")
	private Employee employee;

	//bi-directional many-to-one association to SeminarType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SeminarTypeId")
	private SeminarType seminarType;

	public Seminar() {
	}

	public int getSeminarId() {
		return this.seminarId;
	}

	public void setSeminarId(int seminarId) {
		this.seminarId = seminarId;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getSeminarDate() {
		return this.seminarDate;
	}

	public void setSeminarDate(Date seminarDate) {
		this.seminarDate = seminarDate;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public SeminarType getSeminarType() {
		return this.seminarType;
	}

	public void setSeminarType(SeminarType seminarType) {
		this.seminarType = seminarType;
	}

}