package entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the SeminarTypes database table.
 * 
 */
@Entity
@Table(name="SeminarTypes")
@NamedQuery(name="SeminarType.findAll", query="SELECT s FROM SeminarType s")
public class SeminarType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SeminarTypeId")
	private int seminarTypeId;

	@Column(name="Active")
	private byte active;

	@Column(name="Name")
	private String name;

	@Column(name="Notes")
	private String notes;

	@Column(name="SeminarTypeCode")
	private String seminarTypeCode;

	@JsonIgnore
	//bi-directional many-to-one association to Seminar
	@OneToMany(mappedBy="seminarType")
	private List<Seminar> seminars;

	public SeminarType() {
	}

	public int getSeminarTypeId() {
		return this.seminarTypeId;
	}

	public void setSeminarTypeId(int seminarTypeId) {
		this.seminarTypeId = seminarTypeId;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getSeminarTypeCode() {
		return this.seminarTypeCode;
	}

	public void setSeminarTypeCode(String seminarTypeCode) {
		this.seminarTypeCode = seminarTypeCode;
	}

	public List<Seminar> getSeminars() {
		return this.seminars;
	}

	public void setSeminars(List<Seminar> seminars) {
		this.seminars = seminars;
	}

	public Seminar addSeminar(Seminar seminar) {
		getSeminars().add(seminar);
		seminar.setSeminarType(this);

		return seminar;
	}

	public Seminar removeSeminar(Seminar seminar) {
		getSeminars().remove(seminar);
		seminar.setSeminarType(null);

		return seminar;
	}

}