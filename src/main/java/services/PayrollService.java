package services;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.Dao;
import entities.Employee;
import entities.EmployeePayroll;
import entities.Payroll;

@Service
public class PayrollService {

	@Autowired Dao dao;
	
	public void calculatePayroll(Payroll payroll) {
		dao.deletePayrollRecords(payroll);
		for(Employee employee : dao.getAllEmployees()) {
			EmployeePayroll ep = new EmployeePayroll();
			ep.setEmployee(employee);
			ep.setPayroll(payroll);
			long salary = employee.getSalary().longValueExact();
			ep.setSalary(employee.getSalary());
			long tax;
			if(salary <= 2000) {
				tax = (long) (salary*0.02);
			}
			else if(salary <= 4000) {
				tax = (long) (salary*0.04);
			}	
			else {
				tax = (long) (salary*0.06);
			}
			ep.setTax(BigDecimal.valueOf(tax));
			ep.setNetPayable(BigDecimal.valueOf(salary-tax));
			dao.createEmployeePayroll(ep);
		}
	}
}
