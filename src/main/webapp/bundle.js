var app = angular.module('app',['ui.router']);


app.config(function($stateProvider, $urlRouterProvider){

	$urlRouterProvider.when("","/");
	
	$stateProvider
	.state("home",{
		url:"/",
		templateUrl:"./templates/home.html"
	})
	.state("employees",{
		url:"/employees",
		templateUrl:"templates/employees.html",
		controller:"employeesCtrl",
		resolve:{
			employees:function($http){
				return $http({url:"services/employees",
						method:"GET",
						}).then(function(response){return response.data});
			}
		}
	})
	.state("createEmployee",{
		url:"/employees/create",
		templateUrl:"templates/createEmployee.html",
		controller:"createEmployeeCtrl"
		
	})
	.state("updateEmployee",{
		url:"/employees/:id/update",
		templateUrl:"templates/createEmployee.html",
		controller:"updateEmployeeCtrl",
		resolve:{
			employee:function($http,$stateParams){
				return $http({url:"services/employees/"+$stateParams.id,
						method:"GET"
				  }).then(function(response){return response.data});
			}
		}	
	})
	.state("seminarTypes",{
		url:"/seminarTypes",
		templateUrl:"templates/seminarTypes.html",
		controller:"seminarTypesCtrl",
		resolve:{
			seminarTypes:function($http){
				return $http({url:"services/seminarTypes",
					method:"GET"
			  }).then(function(response){return response.data});
			}
		}
		
	})
	.state("createSeminarType",{
		url:"/seminarTypes/create",
		templateUrl:"templates/createSeminarType.html",
		controller:"createSeminarTypeCtrl"
		
	})
	.state("employeeSeminars",{
		url:"/employees/:id/seminars",
		templateUrl:"templates/seminars.html",
		controller:"employeeSeminarCtrl",
		resolve:{
			employeeSeminars:function($http,$stateParams){
				return $http({url:"services/employees/"+$stateParams.id+"/seminars",
						method:"GET"
			  }).then(function(response){return response.data.map(function(item){item.seminarDate = new Date(item.seminarDate);console.log(item);return item;})});
			}
		}
	})
	.state("createEmployeeSeminar",{
		url:"/employees/:id/createEmployeeSeminar",
		templateUrl:"templates/createEmployeeSeminar.html",
		controller:"createEmployeeSeminarCtrl",
		resolve:{
			seminarTypes:function($http){
				return $http({url:"services/seminarTypes",
					method:"GET"
			  }).then(function(response){return response.data});
			}
		}
	})
	.state("employeePayrolls",{
		url:"/employees/:id/payrolls",
		templateUrl:"templates/employeePayrolls.html",
		controller:"employeePayrollsCtrl",
		resolve:{
			employeePayrolls:function($http,$stateParams){
				return $http({url:"services/employees/"+$stateParams.id+"/payrolls",
					method:"GET"
			  }).then(function(response){return response.data});
			},
			employee:function($http,$stateParams){
				return $http({url:"services/employees/"+$stateParams.id,
						method:"GET"
				  }).then(function(response){return response.data});
			}
		} 
	})
	.state("payrolls",{
		url:"/payrolls",
		templateUrl:"templates/payrolls.html",
		controller:"payrollsCtrl",
		resolve:{
			payrolls:function($http){
				return $http({url:"services/payrolls",
						method:"GET"
				  }).then(function(response){return response.data});
			}
		} 
	})
	.state("createPayroll",{
		url:"/payrolls/create",
		templateUrl:"templates/createPayroll.html",
		controller:"createPayrollCtrl"
	})
	;

});

app.controller("employeesCtrl",function($state,$http,$scope,employees){
	$scope.employees = employees;
	$scope.update = function(id){
		$state.go("updateEmployee",{id:id});
	}
	$scope.delete = function(id){
		$http({url:"services/employees/"+id, method:"DELETE"})
		.then(function(response){
				console.log("succes");	
				$state.go($state.current,{},{reload:true})
			},function(error){
				console.log("error");
		});
	};
});

app.controller("createEmployeeCtrl",function($scope,$state,$http){
	$scope.emp = {};
	$scope.cancel = function(){$state.go("employees");};
	$scope.create = function(){
		$http({url:"services/employees",
				method:"POST",
				data:$scope.emp}).then(function(response){
					$state.go("employees");
				},function(error){
					console.log(error);
				});
	};
});

app.controller("updateEmployeeCtrl",function($scope,$state,$stateParams,$http,employee){
	$scope.emp = employee;
	$scope.cancel = function(){$state.go("employees");};
	$scope.create = function(){
		$http({url:"services/employees/"+$stateParams.id,
				method:"PUT",
				data:$scope.emp}).then(function(response){
					$state.go("employees");
				},function(error){
					console.log(error);
				});
	};
});

app.controller("seminarTypesCtrl",function($state,$http,$scope,seminarTypes){
	$scope.seminarTypes = seminarTypes;
	$scope.delete = function(id){
		$http({url:"services/seminarTypes/"+id, method:"DELETE"})
		.then(function(response){
				console.log("succes");	
				$state.go($state.current,{},{reload:true})
			},function(error){
				console.log("error");
		});
	};
});

app.controller("createSeminarTypeCtrl",function($state,$http,$scope){
	$scope.st = {};
	$scope.cancel = function(){$state.go("seminarTypes");};
	$scope.create = function(){
		$http({url:"services/seminarTypes",
				method:"POST",
				data:$scope.st}).then(function(response){
					$state.go("seminarTypes");
				},function(error){
					console.log(error);
				});
	};
});

app.controller("employeeSeminarCtrl",function($scope,$http,$state,$stateParams,employeeSeminars){
	console.log(employeeSeminars);
	$scope.employeeSeminars = employeeSeminars;
	$scope.emplId = $stateParams.id;
	$scope.delete = function(id){
		$http({url:"services/employees/"+$stateParams.id+"/seminars/"+id, method:"DELETE"})
		.then(function(response){
				console.log("succes");	
				$state.go($state.current,{},{reload:true})
			},function(error){
				console.log("error");
		});
	};
});

app.controller("createEmployeeSeminarCtrl",function($scope,$http,$state,$stateParams,seminarTypes){
	$scope.emplSem = {};
	$scope.seminarTypes = seminarTypes;
	$scope.cancel = function(){$state.go("employeeSeminars",$stateParams);};
	$scope.create = function(){
		$http({url:"services/employees/"+$stateParams.id+"/seminars/"+$scope.semTypId,
				method:"POST",
				data:$scope.emplSem}).then(function(response){
					$state.go("employeeSeminars",$stateParams);
				},function(error){
					console.log(error);
				});
	};
});

app.controller("employeePayrollsCtrl",function($scope,$http,employeePayrolls,employee){
	$scope.employee = employee;
	$scope.employeePayrolls = employeePayrolls;
	console.log(employeePayrolls);
	
});

app.controller("payrollsCtrl",function($http, $scope, payrolls, $state){
	$scope.payrolls = payrolls;
	$scope.delete = function(id){
		$http({url:"services/payrolls/"+id, method:"DELETE"})
		.then(function(response){
				console.log("succes");	
				$state.go($state.current,{},{reload:true})
			},function(error){
				console.log("error");
		});
	};
});

app.controller("createPayrollCtrl",function($scope,$http,$state,$stateParams){
	$scope.payroll = {};
	$scope.cancel = function(){$state.go("payrolls",$stateParams);};
	$scope.create = function(){
		$http({url:"services/payrolls",
				method:"POST",
				data:$scope.payroll}).then(function(response){
					$state.go("payrolls",$stateParams);
				},function(error){
					console.log(error);
				});
	};
});